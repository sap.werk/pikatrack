FROM ruby:2.7.0

ADD . /pikatrack

RUN curl -sL https://deb.nodesource.com/setup_13.x | bash - && \
    apt-get update && \
    apt-get install -y nodejs postgis gpsbabel && \
    npm install -g yarn && \
    cd /pikatrack/backend && \
    bundle install && \
    cd /pikatrack/frontend && \
    yarn install && yarn build