class Api::Admin::UsersController < ApplicationController
    before_action :authorize_admin

    def index
        @users = User.all.order(:id).limit(50).offset((params[:page].to_i - 1) * 50)
        @page_count = (User.count.to_f / 50).ceil
        render 'api/admin/users/index', formats: [:json]
    end

    def lock_user
        User.find(params[:id]).update(locked_at: DateTime.now, tokens: nil)
        head :ok
    end

    def unlock_user
        User.find(params[:id]).update(locked_at: nil)
        head :ok
    end

    def authorize_admin
        authorize! :manage, :all
    end
end
