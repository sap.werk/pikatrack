class Api::PrivacyZoneController < ApplicationController
    before_action :authenticate_api_user!

    def index
        @privacy_zones = PrivacyZone.where(user: current_api_user)
    end

    def create
        @privacy_zone = PrivacyZone.create(
            range: params[:privacy_zone][:range],
            original_point: "POINT(#{params[:privacy_zone][:original_point][1]} #{params[:privacy_zone][:original_point][0]})",
            random_in_range: "POINT(#{params[:privacy_zone][:random_in_range][1]} #{params[:privacy_zone][:random_in_range][0]})",
            user: current_api_user
        )
    end

    def destroy
        PrivacyZone.find_by(user: current_api_user, id: params[:id]).destroy
    end
end
