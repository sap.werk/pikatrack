class Api::ProfileController < ApplicationController
    before_action :authenticate_api_user!

    def show
    end

    def update
        User.find(current_api_user.id).update(user_params)
    end

    def user_params
        params.require(:user).permit(:username, :email, :gender, :settings_unit, :settings_default_sport, :setting_default_privacy)
    end
end
