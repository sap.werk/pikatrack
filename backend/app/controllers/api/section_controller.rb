class Api::SectionController < ApplicationController
    before_action :authenticate_api_user!, only: [:create]

    def show
        @section = Section.find(params[:id])
        render :nothing => true, :status => 404 if @section.nil?
    end

    def create
        @section = Section.create(section_params.merge({user: current_api_user}))
        render 'api/section/create', formats: [:json]
    end

    def section_params
        params.require(:section).permit(:name, :path, :activity_type)
    end
end
