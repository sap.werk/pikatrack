module ApplicationHelper
    def current_unit
        if current_api_user
            return current_api_user.setting_unit
        else
            return "metric"
        end
    end
end