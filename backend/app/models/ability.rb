class Ability
  include CanCan::Ability

  def initialize(user)
      if user&.admin?
          can :manage, :all
      end
      can :manage, Activity, user_id: user&.id
      can :read, Activity, privacy: :public_activity
  end
end
