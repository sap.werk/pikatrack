class ApplicationSetting < ApplicationRecord
    def setting_type
        if !text_value.nil?
            return :text
        elsif !bool_value.nil?
            return :bool
        elsif !number_value.nil?
            return :number
        end
    end
end
