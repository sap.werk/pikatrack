class User < ActiveRecord::Base
    extend Devise::Models
    # Include default devise modules. Others available are:
    # :confirmable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable, :lockable,
           :recoverable, :rememberable, :trackable, :validatable
    include DeviseTokenAuth::Concerns::User

    has_many :activities
    has_many :sections
    has_many :privacy_zones

    enum setting_unit: {metric: 0, imperial: 1}
    enum setting_default_privacy: Activity.privacies
    enum setting_default_sport: Activity.activity_types

    # Override devise method because locks do not expire
    def access_locked?
        locked_at != nil
    end
end
