collection @activities
attributes :id, :title, :description, :user
child :user do
  attributes :name, :username, :gender
end

node(:activity_log_url) { |activity| @activity&.computed_activity&.activity_log_file }
node(:start_time) { |activity| activity.computed_activity.start_time }
node(:end_time) { |activity| activity.computed_activity.end_time }
node(:distance) { |activity| activity.computed_activity.distance.round(2) }
node(:moving_time) { |activity| activity.computed_activity.moving_time }
node(:elevation) { |activity| activity.computed_activity.elevation }
node(:setting_unit) {current_unit}