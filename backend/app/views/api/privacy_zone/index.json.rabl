require 'rgeo/geo_json'

collection @privacy_zones
attributes :id, :range
node(:original_point) {|privacy_zone| RGeo::GeoJSON.encode(privacy_zone.original_point)}
node(:random_in_range) {|privacy_zone| RGeo::GeoJSON.encode(privacy_zone.random_in_range)}
