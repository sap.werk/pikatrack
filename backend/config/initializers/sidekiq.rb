require 'sidekiq'
require 'sidekiq/web'

Sidekiq.default_worker_options = { 'backtrace' => true }
Sidekiq.configure_server do |config|
    config.redis = { url: ENV['REDIS_URL'] }
end
  
Sidekiq.configure_client do |config|
    config.redis = { url: ENV['REDIS_URL'] }
end
Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
    [user, password] == [Rails.application.secrets.sidekiq_username, Rails.application.secrets.sidekiq_password]
end
