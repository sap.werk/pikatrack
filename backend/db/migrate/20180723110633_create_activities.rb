class CreateActivities < ActiveRecord::Migration[5.1]
    def change
        create_table :activities do |t|
            t.string :title, null: false
            t.text :description
            t.timestamp :start_time, null: false
            t.timestamp :end_time, null: false
            t.float :distance
            t.integer :moving_time
            t.float :elevation
            t.references :user
            t.timestamps
        end
    end
end
