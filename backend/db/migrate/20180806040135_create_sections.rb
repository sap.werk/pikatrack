class CreateSections < ActiveRecord::Migration[5.2]
  def change
    create_table :sections do |t|
      t.string :name, index: true, null: false
      t.line_string :path, srid: 3785, null: false, has_z: true

      t.st_point :start_point, geographic: true, using: :gist
      t.float :distance
      t.integer :slope
      t.timestamps
    end
  end
end
