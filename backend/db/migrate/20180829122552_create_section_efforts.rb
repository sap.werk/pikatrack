class CreateSectionEfforts < ActiveRecord::Migration[5.2]
    def change
        create_table :section_efforts do |t|
            t.references :activity
            t.references :section
            t.time :time
            t.timestamps
        end
    end
end
