class RemoveColumnNotNull < ActiveRecord::Migration[5.2]
  def up
    change_column :activities, :title, :string, null: false
    change_column_default :activities, :title, nil
  end

  def down
    change_column :activities, :title, :string, null: false, default: 'Untitled Activity'
  end
end
