require 'rails_helper'

RSpec.describe "Feed API" do
    before(:each) do
        @current_api_user = FactoryBot.create(:user)
    end

    xit 'sends empty feed' do
        login
        auth_params = get_auth_params_from_login_response_headers(response)
        get '/api/feed.json', headers: auth_params
        json = JSON.parse(response.body)
        expect(json.count).to eq(0)
    end

    # it 'sends feed items' do
    #     FactoryBot.create_list(:activity, 10)
    #     p Activity.first
    #     login
    #     auth_params = get_auth_params_from_login_response_headers(response)
    #     get '/feed.json', headers: auth_params
    #     json = JSON.parse(response.body)
    #     p response.body
    #     expect(json.count).to eq(10)
    #
    # end

    def login
        post '/api/auth/sign_in', params:  { email: @current_api_user.email, password: 'password' }.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    end
    def get_auth_params_from_login_response_headers(response)
        {
            'access-token' => response.headers['access-token'],
            'client' => response.headers['client'],
            'uid' => response.headers['uid'],
            'expiry' => response.headers['expiry'],
            'token_type' => response.headers['token-type']
        }
    end

end