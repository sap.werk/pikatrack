FactoryBot.define do
    factory :user do
        uid { "user_#{User.count + 1}@example.com" } # Not sure if this is the best way to create unique emails
        email { "user_#{User.count + 1}@example.com" }
        username { "user_#{User.count + 1}" }
        password { "password" }
    end
end
