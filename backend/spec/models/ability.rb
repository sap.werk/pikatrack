require 'rails_helper'

RSpec.describe Ability, type: :model do
    describe 'activity' do
        it 'allows non owner to view public activity' do
            user = create(:user)
            activity = create(:activity_gpx_file, privacy: :public_activity)
            ability = Ability.new(user)

            expect(ability.can?(:read, activity)).to be_truthy
        end

        it 'blocks non owner from viewing private activity' do
            user = create(:user)
            activity = create(:activity_gpx_file, privacy: :private_activity)
            ability = Ability.new(user)

            expect(ability.can?(:read, activity)).to be_falsy
        end

        it 'allows owner to manage their own activity' do
            user = create(:user)
            activity = create(
                :activity_gpx_file,
                privacy: :public_activity,
                user: user
            )
            ability = Ability.new(user)

            expect(ability.can?(:manage, activity)).to be_truthy
        end
    end

    describe 'admin' do
        it 'allows admin to manage all' do
            user = create(:user, admin: true)
            ability = Ability.new(user)

            expect(ability.can?(:manage, :all)).to be_truthy
        end

        it 'does not allow non admin to manage all' do
            user = create(:user)
            ability = Ability.new(user)

            expect(ability.can?(:manage, :all)).to be_falsy
        end
    end
end
