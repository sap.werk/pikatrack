# Docker hosting
Hosting pikatrack is fairly simple, there is a single container that contains both the app and the worker. Redis, postgres and nginx are also required. 

An example docker-compose and nginx config are provided. It is a requirement that you know how to configure nginx to properly set up pikatrack. To set up pikatrack, simply copy the docker compose and nginx.conf files to your server, edit the docker-compose file to fill in all of the blank environment variables. Now all that has to be done is the nginx configuration. I assume you will likely already have nginx running for other apps so you can remove it from the docker compose file and configure your existing one using the example config as a guide. There is really nothing special to the nginx config.

Once you have the pikatrack image running, run `sudo docker-compose run pikatrack bash -c "cd /pikatrack/backend && RAILS_ENV=production rails db:schema:load"` to create the database schema and then `sudo docker-compose run pikatrack bash -c "cd /pikatrack/backend && RAILS_ENV=production rails db:seed"` to load the default data

You can then create a user for yourself by opening a rails console with `sudo docker-compose run pikatrack bash -c "cd /pikatrack/backend && rails c -e production"` and running `User.create(admin: true, username: 'username', uid: 'email@email.com', email: 'email@email.com', password: 'pass')` inside it (Edit all the values to set them on your user). You can then close the console and log in. You should now head to /admin/application_settings to set up your api keys and other settings.

If you get stuck or have any questions, please feel free to open a bug on gitlab with your configs and setup and I'll do my best to help you get it working.