import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const admin = new Vapi({
    baseURL: '/api',
    state: {
        admin: []
    }
}).get({
    action: 'show_admin',
    property: 'admin',
    path: '/admin.json'
}).getStore({
    createStateFn: true
})

export default {
    state: admin.state,
    mutations: admin.mutations,
    actions: admin.actions,
    getters: admin.getters
}
