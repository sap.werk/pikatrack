import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const users = new Vapi({
    baseURL: '/api',
    state: {
        users: []
    }
}).get({
    action: 'admin_users_index',
    property: 'admin_users',
    path: ({page}) => `/admin/users?page=${page}.json`
}).post({
    action: 'admin_users_lock',
    property: 'admin_users',
    path: ({user_id}) => `/admin/users/${user_id}/lock`
}).post({
    action: 'admin_users_unlock',
    property: 'admin_users',
    path: ({user_id}) => `/admin/users/${user_id}/unlock`
}).getStore({
    createStateFn: true
})

export default {
    state: users.state,
    mutations: users.mutations,
    actions: users.actions,
    getters: users.getters
}
