import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const sectionEfforts = new Vapi({
    baseURL: '/api',
    state: {
        sectionEfforts: []
    }
}).get({
    action: 'show_section_efforts_for',
    property: 'sectionEfforts',
    path: ({id, user_id}) => `/section_efforts.json?section_id=${id}&user_id=${user_id}`
}).get({
    action: 'show_current_user_section_efforts',
    property: 'sectionEfforts',
    path: ({id}) => `/section_efforts.json?section_id=${id}`
}).getStore({
    createStateFn: true
})

export default {
    state: sectionEfforts.state,
    mutations: sectionEfforts.mutations,
    actions: sectionEfforts.actions,
    getters: sectionEfforts.getters
}
