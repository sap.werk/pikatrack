import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const signup = new Vapi({
    baseURL: '/api',
    state: {
        signup: []
    }
}).get({
    action: 'show_signup',
    property: 'signup',
    path: '/auth/new'
}).getStore({
    createStateFn: true
})

export default {
    state: signup.state,
    mutations: signup.mutations,
    actions: signup.actions,
    getters: signup.getters
}
